(ns plf01.clj)


(defn función-<-1
  [a b]
  (< a b))
(defn función-<-2
  [a b c]
  (< a b c))
(defn función-<-3
  [a b c d]
  (< a b c d))
(función-<-1 1 2)
(función-<-2 1 2 5)
(función-<-3 1 2 3 4)

(defn función-<=-1
  [a b]
  (<= a b))
(defn función-<=-2
  [a b c]
  (<= a b c))
(defn función-<=-3
  [a b c d]
  (<= a b c d))
(función-<=-1 1 2)
(función-<=-2 1 2 3)
(función-<=-3 1 2 5 6)

(defn función-==-1
  [a b]
  (== a b))
(defn función-==-2
  [a b c]
  (== a b c))
(defn función-==-3
  [a b c d]
  (== a b c d))
(función-==-1 2 3)
(función-==-2 2 3 6)
(función-==-3 1 1 1 1)

(defn función->-1
  [a b]
  (< a b))
(defn función->-2
  [a b c]
  (< a b c))
(defn función->-3
  [a b c d]
  (< a b c d))
(función->-1 1 2)
(función->-2 1 2 5)
(función->-3 1 2 3 4)

(defn función->=-1
  [a b]
  (<= a b))
(defn función->=-2
  [a b c]
  (<= a b c))
(defn función->=-3
  [a b c d]
  (<= a b c d))
(función->=-1 1 2)
(función->=-2 1 2 3)
(función->=-3 1 2 5 6)

(defn función-assoc-1
  [map a b]
  (assoc map a b))
(defn función-assoc-2
  [vector a b]
  (assoc vector a b))
(defn función-assoc-3
  [vector map b]
  (assoc vector map b))
(función-assoc-1 {:key 2 :key2 3} 7 5)
(función-assoc-2 [2 3 4] 7 5)
(función-assoc-3 [2 3] {:key 7} 5)

(defn función-assoc-in-1
  [vector a b]
  (assoc-in vector [a] b))
(defn función-assoc-in-2
  [vector a]
  (assoc-in vector [a] {}))
(defn función-assoc-in-3
  [vector a map]
  (assoc-in vector [a] map))
(función-assoc-in-1 [1 2 3] 0 5)
(función-assoc-in-2 [2 3 4] 1)
(función-assoc-in-3 [2 3] 1 {:key 7})

(defn función-concat-1
  [a b]
  (concat a b))
(defn función-concat-2
  [[a] [b]]
  (concat [a] [b]))
(defn función-concat-3
  [a [b]]
  (concat a [b]))
(función-concat-1 2 3)
(función-concat-2 [2] [3])
(función-concat-3 2 [3])

(defn función-conj-1
  [vector c]
  (conj vector c))
(defn función-conj-2
  [map c]
  (conj map c))
(defn función-conj-3
  [a b]
  (conj a b))
(función-conj-1 [2 3 4] 5)
(función-conj-2 {:k1 2 :k2 4} 5)
(función-conj-3 6 5)

(defn función-cons-1
  [vector c]
  (cons c vector))
(defn función-cons-2
  [map c]
  (cons c map))
(defn función-cons-3
  [a b]
  (cons a b))
(función-cons-1 [2 3 4] 5)
(función-cons-2 {:k1 2 :k2 4} 5)
(función-cons-3 6 5)

(defn función-contains?-1
  [vector c]
  (contains? vector c))
(defn función-contains?-2
  [map c]
  (contains? map c))
(defn función-contains?-3
  [a b]
  (contains? a b))
(función-contains?-1 [2 3 4] 1)
(función-contains?-2 {:k1 2 :k2 4} 0)
(función-contains?-3 6 5)

(defn función-count-1
  [vector]
  (count vector))
(defn función-count-2
  [map]
  (count map))
(defn función-count-3
  [str]
  (count str))
(función-count-1 [2 3 4])
(función-count-2 {:k1 2 :k2 4})
(función-count-3 "Miau")

(defn función-disj-1
  [vector]
  (disj vector))
(defn función-disj-2
  [map c]
  (disj map c))
(defn función-disj-3
  [map a b]
  (disj map a & b))
(función-disj-1 [2 3 4])
(función-disj-2 {:k1 2 :k2 4} 5)
(función-disj-3 {:k1 2 :k2 4 :k3 7} 0 4)

(defn función-dissoc-1
  [vector]
  (dissoc vector))
(defn función-dissoc-2
  [map c]
  (dissoc map c))
(defn función-dissoc-3
  [map a b]
  (dissoc map a & b))
(función-dissoc-1 [2 3 4])
(función-dissoc-2 {:k1 2 :k2 4} 5)
(función-dissoc-3 {:k1 2 :k2 4 :k3 7} 0 4)

(defn función-distinct-1
  [vector]
  (distinct vector))
(defn función-distinct-2
  [map]
  (distinct map))
(defn función-distinct-3
  [list]
  (distinct list))
(función-distinct-1 [2 3 4])
(función-distinct-2 {:k1 2 :k2 4})
(función-distinct-3 '("d" a v b))

(defn función-distinct?-1
  [a b]
  (distinct? a b))
(defn función-distinct?-2
  [a b c]
  (distinct? a b c))
(defn función-distinct?-3
  [a b c d]
  (distinct? a b c d))
(función-distinct?-1 2 3)
(función-distinct?-2 5 6 8)
(función-distinct?-3 1 1 1 1)

(defn función-drop-last-1
  [vector]
  (drop-last vector))
(defn función-drop-last-2
  [map]
  (drop-last map))
(defn función-drop-last-3
  [a vector]
  (drop-last a vector))
(función-drop-last-1 [2 3])
(función-drop-last-2 {:key1 6 :key2 6 :key3 6})
(función-drop-last-3 1 [1 1 1])

(defn función-empty-1
  [vector]
  (empty vector))
(defn función-empty-2
  [map]
  (empty map))
(defn función-empty-3
  [list]
  (empty list))
(función-empty-1 [2 3])
(función-empty-2 {:key1 6 :key2 6 :key3 6})
(función-empty-3 '(1 [1 1 1]))

(defn función-empty?-1
  [vector]
  (empty? vector))
(defn función-empty?-2
  [map]
  (empty? map))
(defn función-empty?-3
  [list]
  (empty? list))
(función-empty?-1 [2 3])
(función-empty?-2 {:key1 6 :key2 6 :key3 6})
(función-empty?-3 '(1 [1 1 1]))

(defn función-even?-1
  [a]
  (even? a))
(defn función-even?-2
  []
  (filter even? (range 10)))
(defn función-even?-3
  []
  (filter even? (range 20)))
(función-even?-1 3)
(función-even?-2)
(función-even?-3)

(defn función-false?-1
  [a]
  (false? a))
(defn función-false?-2
  [vector]
  (false? vector))
(defn función-false?-3
  [map]
  (false? map))
(función-false?-1 true)
(función-false?-2 [1 2 3])
(función-false?-3 {:key1 2})

(defn función-find-1
  [map a]
  (find map a))
(defn función-find-2
  [vector a]
  (find vector a))
(defn función-find-3
  [map a]
  (+ (find map a) 2))
(función-find-1 {:k1 2 :k2 3} 1)
(función-find-2 [1 2 3] 2)
(función-find-3 {:key1 2} 0)


(defn función-first-1
  [map]
  (first map))
(defn función-first-2
  [vector]
  (first vector))
(defn función-first-3
  [map]
  (+ (first map) 2))
(función-first-1 {:k1 2 :k2 3})
(función-first-2 [1 2 3])
(función-first-3 {:key1 2})


(defn función-flatten-1
  [list]
  (flatten list))
(defn función-flatten-2
  [vector]
  (flatten vector))
(defn función-flatten-3
  [map]
  (flatten map))
(función-flatten-1 '(:k1 4 :k2 8))
(función-flatten-2 [2 [1 2 3]])
(función-flatten-3 {:k1 {:key1 2} :k2 5})

(defn función-frequencies-1
  [list]
  (frequencies list))
(defn función-frequencies-2
  [vector]
  (frequencies vector))
(defn función-frequencies-3
  [map]
  (frequencies map))
(función-frequencies-1 '(:k1 4 :k2 8))
(función-frequencies-2 [2 [1 2 3]])
(función-frequencies-3 {:k1 {:key1 2} :k2 5})

(defn función-get-1
  [list a]
  (get list a))
(defn función-get-2
  [vector a]
  (get vector a))
(defn función-get-3
  [map a]
  (get map a))
(función-get-1 [2 [1 2 3]] 2)
(función-get-2 '(:k1 4 :k2 7) 1)
(función-get-3 {:k1 {:key1 2} :k2 5} 1)

(defn función-get-in-1
  [list a]
  (get-in a list))
(defn función-get-in-2
  [vector a]
  (get-in a vector))
(defn función-get-in-3
  [map a]
  (get-in a map))
(función-get-in-1 [2 [1 2 3]] 2)
(función-get-in-2 '(:k1 4 :k2 7) 1)
(función-get-in-3 {:k1 {:key1 2} :k2 5} 1)

(defn función-into-1
  [list a]
  (into a list))
(defn función-into-2
  [vector a]
  (into a vector))
(defn función-into-3
  [map a]
  (into a map))
(función-into-1 [2 [1 2 3]] 2)
(función-into-2 '(:k1 4 :k2 7) 1)
(función-into-3 {:k1 {:key1 2} :k2 5} 1)

(defn función-key-1
  [list]
  (key list))
(defn función-key-2
  [vector]
  (key vector))
(defn función-key-3
  [map]
  (key map))
(función-key-1 [2 [1 2 3]])
(función-key-2 '(:k1 4 :k2 7))
(función-key-3 {:k1 {:key1 2} :k2 5})

(defn función-keys-1
  [list]
  (keys list))
(defn función-keys-2
  [vector]
  (keys vector))
(defn función-keys-3
  [map]
  (keys map))
(función-keys-1 [2 [1 2 3]])
(función-keys-2 '(:k1 4 :k2 7))
(función-keys-3 {:k1 {:key1 2} :k2 5})

(defn función-max-1
  [list]
  (max list))
(defn función-max-2
  [vector]
  (max vector))
(defn función-max-3
  [map]
  (max map))
(función-max-1 [2 [1 2 3]])
(función-max-2 '(:k1 4 :k2 7))
(función-max-3 {:k1 {:key1 2} :k2 5})

(defn función-merge-1
  [map a]
  (merge map a))
(defn función-merge-2
  [map]
  (merge map))
(defn función-merge-3
  [map]
  (merge map nil))
(función-merge-1 {:k1 23 :k2 44 :k3 55} 5)
(función-merge-2 '(:k1 4 :k2 7))
(función-merge-3 {:k1 {:key1 2} :k2 5})

(defn función-min-1
  [list]
  (min list))
(defn función-min-2
  [vector]
  (min vector))
(defn función-min-3
  [map]
  (min map))
(función-min-1 [2 [1 2 3]])
(función-min-2 '(:k1 4 :k2 7))
(función-min-3 {:k1 {:key1 2} :k2 5})

(defn función-neg?-1
  [a b]
  (neg? (+ a b)))
(defn función-neg?-2
  [a b]
  (neg? (- a b)))
(defn función-neg?-3
  [a b]
  (neg? (/ a b)))
(función-neg?-1 5 6)
(función-neg?-2 0 5)
(función-neg?-3 3 -4)

(defn función-nil?-1
  [vector]
  (nil? vector))
(defn función-nil?-2
  [a b]
  (nil? (- a b)))
(defn función-nil?-3
  [a b]
  (nil? (/ a b)))
(función-nil?-1 [5 6])
(función-nil?-2 0 5)
(función-nil?-3 3 -4)

(defn función-not-empty-1
  [list]
  (not-empty list))
(defn función-not-empty-2
  [vector]
  (not-empty vector))
(defn función-not-empty-3
  [map]
  (not-empty map))
(función-not-empty-1 [])
(función-not-empty-2 '(:k1 4 :k2 7))
(función-not-empty-3 {:k1 {:key1 2} :k2 5})

(defn función-nth-1
  [list a]
  (nth list a))
(defn función-nth-2
  [vector a]
  (nth vector a))
(defn función-nth-3
  [map a]
  (nth map a))
(función-nth-1 [] 1)
(función-nth-2 '(:k1 4 :k2 7) 2)
(función-nth-3 {:k1 {:key1 2} :k2 5} 3)

(defn función-odd?-1
  [list]
  (odd? list))
(defn función-odd?-2
  [vector]
  (odd? vector))
(defn función-odd?-3
  [a b]
  (odd? (+ a b)))
(función-odd?-1 [])
(función-odd?-2 '(:k1 4 :k2 7))
(función-odd?-3 4 3)

(defn función-partition-1
  [list a]
  (partition a list))
(defn función-partition-2
  [vector a b]
  (partition a b vector))
(defn función-partition-3
  [vector a b c]
  (partition a b c vector))
(función-partition-1 [] 2)
(función-partition-2 '(:k1 4 :k2 7) 2 4)
(función-partition-3 [:k1 2 :k2 5 :k3 7] 4 3 5)

(defn función-partition-all-1
  [a]
  (partition-all a))
(defn función-partition-all-2
  [vector a]
  (partition-all a vector))
(defn función-partition-all-3
  [vector a b]
  (partition-all a b vector))
(función-partition-all-1 2)
(función-partition-all-2 '(:k1 4 :k2 7) 2)
(función-partition-all-3 [:k1 2 :k2 5 :k3 7]  3 5)

(defn función-peek-1
  [list]
  (peek list))
(defn función-peek-2
  [vector]
  (peek vector))
(defn función-peek-3
  [map]
  (peek map))
(función-peek-1 '(2))
(función-peek-2 [{'(:k1 4 :k2 7) 2}])
(función-peek-3 [:k1 2 :k2 5 :k3 7])

(defn función-pop-1
  [list]
  (pop list))
(defn función-pop-2
  [vector]
  (pop vector))
(defn función-pop-3
  [map]
  (pop map))
(función-pop-1 '(2))
(función-pop-2 [{'(:k1 4 :k2 7) 2}])
(función-pop-3 [:k1 2 :k2 5 :k3 7])

(defn función-quot-1
  [a b]
  (quot (+ a b) 1))
(defn función-quot-2
  [a b]
  (quot (- a b) 3))
(defn función-quot-3
  [a b]
  (quot (* a b) 3))
(función-quot-1 2 3)
(función-quot-2 4 3)
(función-quot-3 5 6)

(defn función-range-1
  [a]
  (range a))
(defn función-range-2
  [a b]
  (range a b))
(defn función-range-3
  [a b c]
  (range a b c))
(función-range-1 3)
(función-range-2 4 3)
(función-range-3 5 6 9)

(defn función-rem-1
  [a b]
  (rem (+ a b) 1))
(defn función-rem-2
  [a b]
  (rem (- a b) 3))
(defn función-rem-3
  [a b]
  (rem (* a b) 3))
(función-rem-1 2 3)
(función-rem-2 4 3)
(función-rem-3 5 6)

(defn función-repeat-1
  [a b]
  (repeat (+ a b) 1))
(defn función-repeat-2
  [a b]
  (repeat (- a b) 3))
(defn función-repeat-3
  [a b]
  (repeat (* a b) 3))
(función-repeat-1 2 3)
(función-repeat-2 4 3)
(función-repeat-3 5 6)

(defn función-replace-1
  [sorted-map]
  (replace sorted-map))
(defn función-replace-2
  [map vector]
  (replace map vector))
(defn función-replace-3
  [map map]
  (replace map map))
(función-replace-1 {:k1 :k2 :k3 :k4})
(función-replace-2 [{'(:k1 4 :k2 7) 2}] [])
(función-replace-3 [:k1 2 :k2 5 :k3 7] [1 2 3 4])

(defn función-rest-1
  [list]
  (rest list))
(defn función-rest-2
  [vector]
  (rest vector))
(defn función-rest-3
  [map]
  (rest map))
(función-rest-1 {:k1 :k2 :k3 :k4})
(función-rest-2 [{'(:k1 4 :k2 7) 2}])
(función-rest-3 [:k1 2 :k2 5 :k3 7])

(defn función-select-keys-1
  [list vector]
  (select-keys list vector))
(defn función-select-keys-2
  [vector a]
  (select-keys vector a))
(defn función-select-keys-3
  [map vector]
  (select-keys map vector))
(función-select-keys-1 {:k1 :k2 :k3 :k4} [2 3])
(función-select-keys-2 [{'(:k1 4 :k2 7) 2}] [2 3])
(función-select-keys-3 [:k1 2 :k2 5 :k3 7] [2 3])

(defn función-shuffle-1
  [list]
  (shuffle list))
(defn función-shuffle-2
  [vector]
  (shuffle vector))
(defn función-shuffle-3
  [map]
  (shuffle map))
(función-shuffle-1 {:k1 :k2 :k3 :k4})
(función-shuffle-2 [{'(:k1 4 :k2 7) 2}])
(función-shuffle-3 [:k1 2 :k2 5 :k3 7])

(defn función-sort-1
  [list]
  (sort list))
(defn función-sort-2
  [vector]
  (sort < vector))
(defn función-sort-3
  [map]
  (sort > map))
(función-sort-1 {:k1 :k2 :k3 :k4})
(función-sort-2 [{'(:k1 4 :k2 7) 2}])
(función-sort-3 [:k1 2 :k2 5 :k3 7])

(defn función-split-at-1
  [list a]
  (split-at a list))
(defn función-split-at-2
  [vector a]
  (split-at a vector))
(defn función-split-at-3
  [map a]
  (split-at a map))
(función-split-at-1 {:k1 :k2 :k3 :k4} 2)
(función-split-at-2 [{'(:k1 4 :k2 7) 2}] 3)
(función-split-at-3 [:k1 2 :k2 5 :k3 7] 1)

(defn función-str-1
  [a]
  (str 1 a))
(defn función-str-2
  [a]
  (str 'hola a))
(defn función-str-3
  [a]
  (str a))
(función-str-1 {})
(función-str-2 24)
(función-str-3 [:k1 2 :k2 5 :k3 7])

(defn función-subs-1
  [a b]
  (subs (str 1 a) b))
(defn función-subs-2
  [a b c]
  (subs a b c))
(defn función-subs-3
  [a b]
  (subs a b))
(función-subs-1 1 "Ejem")
(función-subs-2 "24" "Hola" 2)
(función-subs-3 "Miau" 1)

(defn función-subvec-1
  [a b]
  (subvec [a b] 2))
(defn función-subvec-2
  [a b c]
  (subvec a b c))
(defn función-subvec-3
  [a b]
  (subvec a b))
(función-subvec-1 1 "Ejem")
(función-subvec-2 "24" "Hola" 2)
(función-subvec-3 "Miau" 1)

(defn función-take-1
  [list a]
  (take a list))
(defn función-take-2
  [vector a]
  (take a vector))
(defn función-take-3
  [map a]
  (take a map))
(función-take-1 {:k1 :k2 :k3 :k4} 2)
(función-take-2 [{'(:k1 4 :k2 7) 2}] 3)
(función-take-3 [:k1 2 :k2 5 :k3 7] 1)